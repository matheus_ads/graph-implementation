import unittest
from graph import Graph


class GraphTest(unittest.TestCase):

    def setUp(self):
        self.nodes = {0: set(), 1: set(), 2: set(), 3: set(), 4: set(), 5: set(), 6: set(), 7: set(), 8: set()}
        self.graph = Graph(self.nodes)
        self.empty_graph = Graph()

    def test_add_vertex(self):
        # teste no grafo populado
        self.graph.add_vertex(9)
        self.assertEqual(len(self.graph.vertex), 10)
        self.assertIn(9, self.graph.vertex.keys())

        # teste no grafo vazio
        self.empty_graph.add_vertex(1)
        self.assertEqual(len(self.empty_graph.vertex), 1)
        self.assertIn(1, self.empty_graph.vertex.keys())

    def test_remove_vertex(self):
        self.graph.remove_vertex(0)
        self.assertEqual(8, len(self.graph.vertex))
        self.assertNotIn(0, self.graph.vertex)

    def test_disconnect_vertex(self):
        self.graph.add_vertex_with_connections({9: {0, 1, 2}})
        self.graph.add_vertex_with_connections({0: {9, 1, 2}})
        self.graph.disconnect(9, 0)
        self.assertNotIn(0, self.graph.vertex[9])
        self.assertNotIn(9, self.graph.vertex[0])

    def test_connect_vertex(self):
        self.graph.connect(0, 1)
        self.assertIn(1, self.graph.vertex[0])
        self.assertIn(0, self.graph.vertex[1])

    def test_get_order(self):
        self.assertEqual(self.graph.order(), 9)

    def test_get_vertices(self):
        self.assertEqual(self.nodes.keys(), self.graph.vertices())

    def test_get_a_vertex(self):
        self.assertIn(self.graph.get_vertex(), self.graph.vertex)

    def test_adjacent(self):
        self.empty_graph.add_vertex_with_connections({1: {0, 2, 3}})
        self.assertEqual({0, 2, 3}, self.empty_graph.adjacency(1))

    def test_degree(self):
        self.empty_graph.add_vertex_with_connections({1: {0, 2, 3}})
        self.assertEqual(3, self.empty_graph.degree(1))
        self.assertEqual(0, self.graph.degree(1))

    def test_is_regular(self):
        self.assertTrue(self.graph.is_regular())
        # teste false
        self.empty_graph.add_vertex_with_connections({1: {0, 2, 3}})
        self.empty_graph.add_vertex_with_connections({2: {3}})
        self.assertFalse(self.empty_graph.is_regular())

    def test_is_complete(self):
        self.empty_graph.add_vertex_with_connections({1: {2, 3}})
        self.empty_graph.add_vertex_with_connections({2: {3}})
        self.assertTrue(self.empty_graph.is_complete())
        self.assertFalse(self.graph.is_complete())

    def test_transitive_closure(self):
        self.empty_graph.add_vertex_with_connections({1: {2, 3}})
        self.empty_graph.add_vertex_with_connections({3: {4, 5}})
        self.assertEqual({1, 2, 3, 4, 5}, self.empty_graph.transitive_closure(4))
        self.assertEqual({1}, self.graph.transitive_closure(1))
        self.assertNotEqual({1, 2, 3}, self.graph.transitive_closure(3))

    def test_connected_graph(self):
        self.assertEqual(False, self.graph.is_connected())
        self.empty_graph.add_vertex_with_connections({1: {2, 3}})
        self.empty_graph.add_vertex_with_connections({3: {4, 5}})
        self.assertEqual(True, self.empty_graph.is_connected())

    def test_is_tree(self):
        self.empty_graph.add_vertex_with_connections({1: {2, 3}})
        self.empty_graph.add_vertex_with_connections({2: {3}})
        self.assertFalse(self.empty_graph.is_tree())
        self.graph.add_vertex_with_connections({0: {1, 2, 3, 4, 5, 6, 7, 8}})
        self.assertTrue(self.graph.is_tree())

if __name__ == '__main__':
    unittest.main()