from random import choice

class Graph:
    def __init__(self, vertex=None):
        self.vertex = vertex or dict()

    # acoes basicas
    def add_vertex(self, v):
        """Adiciona um novo vertice ao grafo."""
        if v not in self.vertex:
            self.vertex[v] = set()

    def remove_vertex(self, v):
        """Remove o vertice do grafo e as suas conexoes."""
        if v in self.vertex:
            for adj in self.vertex[v]:
                self.disconnect(v, adj)
            del self.vertex[v]

    def connect(self, v1, v2):
        """Conecta os vertices v1 e v2"""
        if v1 in self.vertex and v2 in self.vertex:
            self.vertex[v1].add(v2)
            self.vertex[v2].add(v1)

    def disconnect(self, v1, v2):
        """Desconecta os vertices v1 e v2 do grafo."""
        if v1 in self.vertex and v2 in self.vertex:
            self.vertex[v1].remove(v2)
            self.vertex[v2].remove(v1)

    def order(self):
        """Retorna a ordem do grafo, ou seja a qtde de vertices"""
        return len(self.vertex)

    def vertices(self):
        """Retorna todos os vertices do grafo"""
        return set(self.vertex.keys())

    def get_vertex(self):
        """Retorna aleatoriamente vertice do grafo"""
        return choice(list(self.vertex.keys()))

    def adjacency(self, v):
        """Retorna os vertices adjacentes a v caso nao tenha adjacencias retorna um
           conjunto com o proprio v para ser possivel calcular o fecho transitivo corretamente"""
        if v in self.vertex and self.vertex[v] is not None:
            return self.vertex[v]
        return {v}

    def degree(self, v):
        """Retorna o numero de vertices adjacentes a v."""
        if v in self.vertex:
            return len(self.vertex[v])

    # acoes derivadas
    def is_regular(self):
        """Retorna True se todos os vertices do grafo possuem o mesmo grau"""
        degree_base = self.degree(self.get_vertex()) # pega o grau de um vertice qualquer para efeito de comparacao
        for v in self.vertex:
            if self.degree(v) != degree_base:
                return False
        return True

    def is_complete(self):
        """Retorna True se todos os vertices estao conectados entre si"""
        degree = self.order() -1
        for v in self.vertex:
            if self.degree(v) != degree:
                return False

        return True

    def transitive_closure(self, v, visited=None):
        """Retorna todos os vertices que sao
        transitivamente alcancaveis a partir de v"""
        visited = visited or set()
        visited.add(v)

        for adj in self.adjacency(v):
            if adj not in visited:
                self.transitive_closure(adj, visited)

        return visited

    def is_connected(self):
        """Retorna True se o grafo eh conexo"""
        return self.vertices() == self.transitive_closure(self.get_vertex())

    def is_tree(self):
        """Verifica se o grafo eh uma arvore,
        ou seja se nao possui ciclos e eh conexo"""
        v = self.get_vertex()
        return self.is_connected() and not self.has_cycle(v, v, set())

    def has_cycle(self, v, v_previous, visited):
        """Checa se o vertice v faz parte de um ciclo"""
        if v in visited:
            return True

        visited.add(v)
        for adj in self.adjacency(v):
            if adj != v_previous:
                if self.has_cycle(adj, v, visited):
                    return True

        visited.remove(v)
        return False

    # auxiliares para testes
    def add_vertex_with_connections(self, v):
        """adiciona os vertices no grafo e ja conecta
        ex: v = {0: {1, 2, 3, 4}}"""
        for k in v.keys():
            self.add_vertex(k)
            for i in v[k]:
                self.add_vertex(i)
                self.connect(i, k)
